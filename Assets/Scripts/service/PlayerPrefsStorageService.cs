﻿using UnityEngine;

// simple facade for access to PlayerPrefs
public static class PlayerPrefsStorageService
{
    private const string SOUND_KEY = "sound";

    public static float SoundVolume
    {
        get
        {
            return PlayerPrefs.GetFloat(SOUND_KEY, 1f);
        }
    }

    public static void SaveSoundVolume(float value)
    {
        PlayerPrefs.SetFloat(SOUND_KEY, value);
    }
}