﻿using UnityEngine;

public class BaseLevelController : MonoBehaviour
{
    [SerializeField] private AudioListener AudioListener;

    protected SceneLoaderController SceneLoader;

    private void Start()
    {
        OnStart();
    }

    protected virtual void OnStart()
    {
        // also loading scene possible start with subscription on event with custom event manager, 
        // but in this simple case i just write singleton
        SceneLoader = SceneLoaderController.Instance;

        // i chose playerprefs to pass data between scenes and save between sessions 
        // and don't write static access point script for service and scene load manager
        UpdateSoundValume(PlayerPrefsStorageService.SoundVolume);
    }

    protected void UpdateSoundValume(float value)
    {
        AudioListener.volume = value;
    }
}