﻿using UnityEngine;

public class MainMenuController : BaseLevelController
{
    [SerializeField] private Canvas MainCanvas;
    [SerializeField] private Canvas PopupCanvas;
    [SerializeField] private SettingsPopupView SettingsPopup;

    protected override void OnStart()
    {
        base.OnStart();

        SettingsPopup.OnSave += OnSettingsSave;
        SettingsPopup.OnClosed += OnSettingsPopupClosed;
    }

    public void LoadRoomLevel()
    {
        SceneLoader.LoadLevel(1);
    }

    public void OpenSettingsPopup()
    {
        SettingsPopup.Setup(PlayerPrefsStorageService.SoundVolume);
        SettingsPopup.Show();

        PopupEnabled(true);
    }

    private void OnSettingsSave(float value)
    {
        PlayerPrefsStorageService.SaveSoundVolume(value);
        UpdateSoundValume(value);
    }

    private void OnSettingsPopupClosed()
    {
        PopupEnabled(false);
    }

    private void PopupEnabled(bool value)
    {
        MainCanvas.enabled = !value;
        PopupCanvas.enabled = value;
    }
}