﻿using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

[RequireComponent(typeof(EventTrigger))]
public class RotationAnimationController : MonoBehaviour
{
    [SerializeField] private Vector3 Punch = new Vector3(0f, 0f, 5f);
    [SerializeField] private float Duration = 0.25f;

    private Tweener CurTweener;

    public void OnPointerEnter()
    {
        if (CurTweener != null && CurTweener.active)
            return;

        CurTweener = transform.DOPunchRotation(Punch, Duration);
    }
}
