﻿using System;
using UnityEngine;
using DG.Tweening;

public class GameBoardView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer Highlight;
    [SerializeField] private BallView[] Balls;
    [SerializeField] private AudioClip[] BallAudio;
    [SerializeField] private AudioClip ErrorSound;
    [SerializeField] private AudioSource AudioSource;

    public event Action<int> OnBallSelected;

    public int BallsAmount {            // amount of balls on game field
        get
        {
            return Balls.Length;
        }
    }

    public bool UserInputEnable {       // switch user input ability
        set
        {
            foreach(BallView ball in Balls)
            {
                if (value)
                    ball.OnBallSelected += BallSelected;
                else
                    ball.OnBallSelected -= BallSelected;
            }
        }
    }

    public void ActivateBallBy(int idx)
    {
        BallView ball = Balls[idx];
        AudioClip clip = BallAudio[idx];

        ShowHighlightFor(ball);
        PlayAudioClip(clip);
    }

    public void BallSelected(BallView ball)
    {
        int idx = Array.IndexOf(Balls, ball);
        OnBallSelected?.Invoke(idx);
    }

    public void ErrorAction()
    {
        PlayAudioClip(ErrorSound);
    }

    private void ShowHighlightFor(BallView ball)
    {
        Highlight.transform.localPosition = ball.transform.localPosition;

        // animate hightlight alpha
        Color tmp = Highlight.color;
        tmp.a = 1f;
        DOTween.To(() => tmp.a, x => Highlight.color = new Color(tmp.r, tmp.g, tmp.b, x), 0f, 0.5f);
    }

    private void PlayAudioClip(AudioClip clip)
    {
        AudioSource.Stop();
        AudioSource.clip = clip;
        AudioSource.Play();
    }
}
