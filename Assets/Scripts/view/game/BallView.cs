﻿using System;
using UnityEngine;

public class BallView : MonoBehaviour
{
    public event Action<BallView> OnBallSelected;

    private void OnMouseDown()
    {
        OnBallSelected?.Invoke(this);
    }
}
