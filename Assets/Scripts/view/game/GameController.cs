﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameBoardView GameView;
    [SerializeField] private TextMeshProUGUI ScoreLabel;
    [SerializeField] private int ScoreToWin = 9;          // amount completed rounds after that player go to prev scene
    [SerializeField] private int StartCombination = 3;    // amount balls in start combination
    [SerializeField] private float StartRoundDelay = 2f;  // delays for playing the combination
    [SerializeField] private float NextBallDelay = 1f;
    [SerializeField] private UnityEvent OnGameWin;

    private int CurScore;
    private List<int> CurCombination;       // active combination, that user must repeat
    private int UserCombinationLength;      // length of correct part of  repeated combination by user

    private void Start()
    {
        Init();
        Setup();
        StartNextRound();
    }

    private void Init()
    {
        CurCombination = new List<int>();
    }

    private void Setup()
    {
        GameView.OnBallSelected += OnUserSelectBall;
    }

    private void OnUserSelectBall(int idx)
    {
        IsBallCorrect(idx);
        ChechEndRound();
    }

    private void IsBallCorrect(int checkIdx)
    {
        int rightIdx = CurCombination[UserCombinationLength];

        if (rightIdx == checkIdx)       // compare user choice with ball in sequence
        {
            UserCombinationLength++;
            GameView.ActivateBallBy(rightIdx);
        }
        else
        {
            UserCombinationLength = 0;
            GameView.ErrorAction();

            PlayRound();
        }
    }

    private void ChechEndRound()
    {
        if (UserCombinationLength != CurCombination.Count)
            return;

        SetupNextRound();

        if (ScoreToWin == CurScore)
            OnGameWin?.Invoke();
        else
            StartNextRound();
    }

    private void SetupNextRound()
    {
        UserCombinationLength = 0;
        CurScore++;
        ScoreLabel.text = CurScore.ToString();
    }

    private void StartNextRound()
    {
        PrepareNewCombination();
        PlayRound();
    }

    private void PlayRound()
    {
        StartCoroutine(Play());
    }

    private IEnumerator Play()
    {
        GameView.UserInputEnable = false;

        yield return new WaitForSeconds(StartRoundDelay);

        foreach (int idx in CurCombination)
        {
            GameView.ActivateBallBy(idx);
            yield return new WaitForSeconds(NextBallDelay);
        }

        GameView.UserInputEnable = true;
    }

    private void PrepareNewCombination()
    {
        CurCombination.Clear();

        int curCombinationLength = StartCombination + CurScore;
        for (int i = 0; i < curCombinationLength; i++)
        {
            int ballId = Random.Range(0, GameView.BallsAmount);
            CurCombination.Add(ballId);
        }
    }
}
