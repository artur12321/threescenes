﻿using UnityEngine;

public class MouseLookController : MonoBehaviour
{
    [SerializeField] private Transform Body;
    [SerializeField] private float Speed;

    private float RotationX;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * Speed * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * Speed * Time.deltaTime;

        RotationX -= mouseY;
        RotationX = Mathf.Clamp(RotationX, -90f, 90f);

        transform.localRotation = Quaternion.Euler(RotationX, 0f, 0f);
        Body.Rotate(Vector3.up * mouseX);
    }

    private void OnDestroy()
    {
        Cursor.lockState = CursorLockMode.None;
    }
}
