﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class SettingsPopupView : MonoBehaviour
{
    [SerializeField] private Slider SoundSlider;
    [SerializeField] private TextMeshProUGUI Value;

    public event Action<float> OnSave;
    public event Action OnClosed;

    private Tween Tween;

    public void Setup(float soundValume)
    {
        SoundSlider.value = soundValume;
    }

    public void UpdateSoundValueLabel(float newValue)
    {
        int percent = Mathf.RoundToInt(newValue * 100f);
        Value.text = string.Format("{0}%", percent);
    }

    public void Show()
    {
        // setup show animation
        transform.localScale = new Vector3(0.1f, 0.1f, 1f);
        Tween = transform.DOScale(new Vector3(1f, 1f, 1f), 0.25f).SetAutoKill(false);
    }

    public void OnSaveSettings()
    {
        OnSave?.Invoke(SoundSlider.value);
    }

    public void OnClose()
    {
        Tween.onRewind += CloseAnimCompleted;
        Tween.PlayBackwards();
    }

    private void CloseAnimCompleted()
    {
        Tween.Kill();
        Tween = null;

        OnClosed?.Invoke();
    }
}
