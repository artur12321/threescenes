﻿using UnityEngine;
using UnityEngine.Events;

// simple interface for setup trigger events in editor
public class TriggerEvents : MonoBehaviour
{
    [SerializeField] private UnityEvent OnEnter;
    [SerializeField] private UnityEvent OnExit;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEvents::OnTriggerEnter");

        OnEnter?.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("TriggerEvents::OnTriggerExit");

        OnExit?.Invoke();
    }
}
