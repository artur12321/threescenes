﻿using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

// door task i separate on activation part and pass  through part, use two triggers for each
public class DoorController : MonoBehaviour
{
    [SerializeField] private ParticleSystem Smoke;
    [SerializeField] private AudioSource Sound;
    [SerializeField] private UnityEvent OnPassThrough;

    private bool IsPassAvailable;

    public void DoorActivated(bool value)
    {
        IsPassAvailable = value;

        if (value)
        {
            StartSound();
            Smoke.Play();
        }
        else
        {
            FadeOutSound();
            Smoke.Stop();
        }
    }

    public void TryPassThrough()
    {
        if (IsPassAvailable)
            OnPassThrough?.Invoke();
    }

    private void StartSound()
    {
        DOTween.KillAll();

        Sound.volume = 1f;
        Sound.Play();
    }

    private void FadeOutSound()
    {
        DOTween.To(() => Sound.volume, x => Sound.volume = x, 0f, 1f).OnComplete(Sound.Stop);
    }
}
