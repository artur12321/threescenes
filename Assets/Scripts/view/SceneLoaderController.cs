﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class SceneLoaderController : MonoBehaviour
{
    public static SceneLoaderController Instance { get; private set; }

    [SerializeField] private Canvas Canvas;
    [SerializeField] private CanvasGroup CanvasGroup;

    private int nextSceneId;

    private void Awake()
    {
        SceneLoaderController.Instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // starts scene loading with preloder
    public void LoadLevel(int id)
    {
        nextSceneId = id;
        ShowLoader();
    }

    private void ShowLoader()
    {
        LoaderEnabled(true);

        DOTween.To(() => CanvasGroup.alpha, x => CanvasGroup.alpha = x, 1f, 0.5f)
            .OnComplete(StartLoading);
    }

    private void StartLoading()
    {
        StartCoroutine("LoadSceneAsync");
    }

    private IEnumerator LoadSceneAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(nextSceneId);
        while (!operation.isDone)
        {
            yield return null;
        }

        HideLoader();
    }

    private void HideLoader()
    {
        DOTween.To(() => CanvasGroup.alpha, x => CanvasGroup.alpha = x, 0f, 0.5f)
            .OnComplete(() => LoaderEnabled(false));
    }

    private void LoaderEnabled(bool value)
    {
        Canvas.enabled = value;
        CanvasGroup.blocksRaycasts = value;
    }
}